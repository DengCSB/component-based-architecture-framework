﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CBABasicFramework
{
    class Program
    {
        static void Main(string[] args)
        {
            Scene.Current = new Scene();
            GameObject go = Scene.Current.CreateObject();
            go.AddComponent<TestMovement>();

            Scene.Current.Start();
        }
    }
}
